const button = document.getElementById('draw');
const circleDiameter = document.createElement('input');
const circleColor =document.createElement('input');
const circleDraw =document.createElement('button');
const circles = document.createElement('div');
button.style.display = 'inline-block';
button.style.width ='150px';
button.style.height ='50px';
button.style.borderRadius = '10px';
circleDiameter.id ='diameter';
circleDiameter.placeholder = 'Enter the circle diameter in pixels';
circleDiameter.style.width = '250px';
circleDiameter.style.marginRight = '20px';
circleDraw.id ='circle-draw';
circleDraw.style.display = 'block';
circleDraw.style.width ='150px';
circleDraw.style.height ='50px';
circleDraw.style.borderRadius = '10px';
circleDraw.style.marginTop = '20px';
circleDraw.innerText = 'Draw';
circles.id = "circles";
circles.style.display = 'flex';
circles.style.flexWrap ='wrap';

function validate (element){
    if (element.className === 'warning') {
        element.className = '';
        document.body.removeChild(document.body.children[2]);
    }

    if (isNaN(+element.value)|| +element.value < 1) {
        element.classList.add('warning');
        const msgElem = document.createElement('p');
        msgElem.className = 'warning-text';
        msgElem.innerHTML = `It isn't a number`;
        document.body.insertBefore(msgElem, document.getElementById('circle-draw'));
    }
}

document.addEventListener('click', function (e) {
    if (e.target.id === 'draw') {
        button.parentElement.removeChild(button);
        document.body.appendChild(circleDiameter);
        document.body.appendChild(circleDraw);
    }

    if (e.target.id === 'circle-draw'){
        validate(document.getElementById('diameter'));
        const diameterValue = +document.getElementById('diameter').value;
        document.body.appendChild(circles);
        circles.style.width = `${diameterValue*10}px`;

        for (let i = 1; i<=100; i++) {
            const circle = document.createElement("div");
            let x = Math.floor(Math.random()*256);
            let y = Math.floor(Math.random()*256);
            let z = Math.floor(Math.random()*256);
            circle.style.backgroundColor = `rgb(${x},${y},${z})`;
            circle.style.borderRadius = '50%';
            circle.style.width = diameterValue + 'px';
            circle.style.height = diameterValue + 'px';
            circle.style.marginTop = '20px';
            circle.classList.add("circle");
            document.getElementById("circles").appendChild(circle);
        }
    }

    let target = event.target;
    while (target !== document) {
        if (target.className === 'circle') {
            target.hidden = true;
            return;
        }
        target = target.parentNode;
    }
});